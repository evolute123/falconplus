Rails.application.routes.draw do

  root 'home#index'
  devise_for :users

  namespace :epsadmin do 
    resources :merchants do
    get '/merchant_status' => "merchants#merchant_status_check"
     get '/transaction' => "merchants#transaction",:as => :transaction
    get '/transaction_status' => "merchants#transaction_status_check"
      resources :invoices
     get '/invoice_status' => "invoices#invoice_status_check"
      resources :devices do 
         get '/device_status' => "devices#device_status_check"
      end
    end

    post 'admins/update_user_status' => 'admins#update_user_status'
    resources :admins
  end

  get 'admin_dashboard/all_device'
  get 'admin_dashboard/all_invoice'
  get 'admin_dashboard/individual_device/:id' => "admin_dashboard#individual_device",:as => :individual_device, via: [:get,:post]

  get 'dashboard/index'
  get 'dashboard/transaction'
  get 'dashboard/billing'
  get '/dashboard/billing_individual/:id' => 'dashboard#billing_individual', :as => :dashboard_billing_individual 
  get 'dashboard/merchant_detail'
  post '/dashboard/update_transaction' => 'dashboard#update_transaction'
  get '/dashboard/transaction_cancel/:id' => 'dashboard#transaction_cancel', :as => :dashboard_transaction_cancel


  ##API RELATED ROUTES
  namespace :api do
    namespace :v1 do
      #get "/falconplus/validate_device" => "falcon_plus#validate_device"
      post "/falconplus/send_otp"=>"falcon_plus#send_otp"
      post "/falconplus/save_customer"=>"falcon_plus#save_customer"
      post "/falconplus/create_transaction"=>"falcon_plus#create_transaction"
      get "/falconplus/get_transaction_by_id"=>"falcon_plus#get_transaction_by_id"
      post "/falconplus/update_transaction_by_id"=>"falcon_plus#update_transaction_by_id"
      #get "/falconplus/pending_transaction_by_code"=>"falcon_plus#pending_transaction_by_code"
      #get "/falconplus/pending_transaction_by_terminal_id"=>"falcon_plus#pending_transaction_by_terminal_id"
      #get "/falconplus/pending_transaction_by_id"=>"falcon_plus#pending_transaction_by_id"
      
     # match "/falconplus/update_transaction_by_terminal_id"=>"falcon_plus#update_transaction_by_terminal_id", via: [:put,:post]
    end
  end
end
