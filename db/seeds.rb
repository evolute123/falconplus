# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#User.create(:full_name => 'Super Admin',:email => 'epsadmin@gmail.com',:password => 'epsadmin1',:role => 'superadmin')
Transaction.create(:invoice_number => '4EC05302752', :amount => '2999', :transaction_status => 'active', :device_id => '574e9e191d41c80f96000000', :merchant_id => '574d84071d41c85eed000002', :customer_id => '125890', :transaction_timestamp => '25/01/2016')
Transaction.create(:invoice_number => '4EC05302752', :amount => '1678', :transaction_status => 'pending', :device_id => '574e9e191d41c80f96000000', :merchant_id => '574d84071d41c85eed000002', :customer_id => '125890', :transaction_timestamp => '25/02/2016')
Transaction.create(:invoice_number => '4EC05302752', :amount => '4975', :transaction_status => 'pending', :device_id => '574e9e191d41c80f96000000', :merchant_id => '574d84071d41c85eed000002', :customer_id => '125890', :transaction_timestamp => '25/03/2016')
Transaction.create(:invoice_number => '4EC05302752', :amount => '2524', :transaction_status => 'pending', :device_id => '574e9e191d41c80f96000000', :merchant_id => '574d84071d41c85eed000002', :customer_id => '125890', :transaction_timestamp => '25/03/2016')
Transaction.create(:invoice_number => '4EC05302752', :amount => '1870', :transaction_status => 'pending', :device_id => '574e9e191d41c80f96000000', :merchant_id => '574d84071d41c85eed000002', :customer_id => '125890', :transaction_timestamp => '25/04/2016')
Transaction.create(:invoice_number => '4EC05302752', :amount => '540', :transaction_status => 'pending', :device_id => '574e9e191d41c80f96000000', :merchant_id => '574d84071d41c85eed000002', :customer_id => '125890', :transaction_timestamp => '25/04/2016')
Transaction.create(:invoice_number => '4EC05302752', :amount => '2040', :transaction_status => 'active', :device_id => '574e9e191d41c80f96000000', :merchant_id => '574d84071d41c85eed000002', :customer_id => '125890', :transaction_timestamp => '02/05/2016')
Transaction.create(:invoice_number => '4EC05302752', :amount => '1540', :transaction_status => 'pending', :device_id => '574e9e191d41c80f96000000', :merchant_id => '574d84071d41c85eed000002', :customer_id => '125890', :transaction_timestamp => '25/05/2016')
Transaction.create(:invoice_number => '4EC05302752', :amount => '4999', :transaction_status => 'pending', :device_id => '574e9e191d41c80f96000000', :merchant_id => '574d84071d41c85eed000002', :customer_id => '125890', :transaction_timestamp => '25/05/2016')
Transaction.create(:invoice_number => '4EC05302752', :amount => '1000', :transaction_status => 'pending', :device_id => '574e9e191d41c80f96000000', :merchant_id => '574d84071d41c85eed000002', :customer_id => '125890', :transaction_timestamp => '26/05/2016')
Transaction.create(:invoice_number => '4EC05302752', :amount => '299', :transaction_status => 'pending', :device_id => '574e9e191d41c80f96000000', :merchant_id => '574d84071d41c85eed000002', :customer_id => '125890', :transaction_timestamp => '27/05/2016')
Transaction.create(:invoice_number => '4EC05302752', :amount => '400', :transaction_status => 'pending', :device_id => '574e9e191d41c80f96000000', :merchant_id => '574d84071d41c85eed000002', :customer_id => '125890', :transaction_timestamp => '28/05/2016')
Transaction.create(:invoice_number => '4EC05302752', :amount => '999', :transaction_status => 'pending', :device_id => '574e9e191d41c80f96000000', :merchant_id => '574d84071d41c85eed000002', :customer_id => '125890', :transaction_timestamp => '29/05/2016')
Transaction.create(:invoice_number => '4EC05302752', :amount => '1299', :transaction_status => 'pending', :device_id => '574e9e191d41c80f96000000', :merchant_id => '574d84071d41c85eed000002', :customer_id => '125890', :transaction_timestamp => '30/05/2016')