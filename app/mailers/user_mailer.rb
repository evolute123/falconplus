class UserMailer < ApplicationMailer
 
  def sms_user(user)
	  @email = user.email
	  @password = user.password
    mail(:to => user.email, :subject => "Welcome Email", :from => "test@yopmail.com") 
  end

  def welcome_admin(admin)
  	@name = admin.full_name
	  @email = admin.email
    @password = admin.password
    mail(:to => admin.email, :subject => "Welcome Email", :from => "test@yopmail.com")
  end

end
