class InvoiceLineItem
  include Mongoid::Document
  include Mongoid::History::Trackable
  field :description, type: String
  field :amount, type: Integer
  field :line_item_type, type: String
  field :invoice_id, type: String

  belongs_to :invoice
   track_history   :on => [:description,:amount,:line_item_type,:invoice_id],      
  :modifier_field => :modifier,
  :version_field => :version,  
  :track_create   =>  true,   
  :track_update   =>  true,   
  :track_destroy  =>  true    
end
