class Transaction
  include Mongoid::Document
  field :invoice_number, type: Integer
  field :amount, type: Integer
  field :transaction_type, type: String
  field :transaction_timestamp, type: DateTime
  field :payment_timestamp, type: DateTime
  field :transaction_details, type: String
  field :transaction_status, type: String
  field :device_id, type: String
  field :merchant_id, type: String
  field :customer_id, type: String
  # field :state, type: String

  belongs_to :device
  belongs_to :merchant
  belongs_to :customer

  state_machine :transaction_status, initial: :pending do
    before_transition :to => :paid, :from => [:pending,:timeout], :do => [:transaction_field_validation]
    event :pay do
      transition :pending => :paid
    end
    event :cancel do
      transition :pending => :cancelled
    end
    event :decline do
      transition :pending => :declined
    end
    event :timeout do
      transition :pending => :timeout
    end
    event :pay do
      transition :timeout => :paid
    end
    event :cancel do
      transition :timeout => :cancelled
    end
    #testing
    event :pending do
      transition :paid => :pending
    end
  end
  def transaction_field_validation
    validates_presence_of  :transaction_details,:payment_timestamp
  end

end
