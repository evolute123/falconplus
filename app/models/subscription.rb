class Subscription
  include Mongoid::Document
  include Mongoid::History::Trackable
  field :start_subscription_date, type: String
  field :monthly_invoice_date, type: String
  field :merchant_id, type: String

  has_many :invoices, :dependent => :destroy
  belongs_to :merchant
   # track_history   :on => [:start_subscription_date,:monthly_invoice_date,:merchant_id],     
   #                :modifier_field => :modifier,
                
   #                :version_field => :version,  
   #                :track_create   =>  true,   
   #                :track_update   =>  true,    
   #                :track_destroy  =>  true    
end
