class AdminDashboardController < ApplicationController

  before_filter :check_not_merchant
  before_action :get_merchant

  def all_device
    @all_device = Device.all
    respond_to do |format|
      format.html
      format.json { render json: @all_device }
    end
  end

  def individual_device
    @individual_device = Device.find(params[:id])
    respond_to do |format|
      format.html
      format.json { render json: @individual_device }
    end
  end

  #For admin viewing all invoices
  def all_invoice
    @all_invoice = Invoice.all
    respond_to do |format|
      format.html
      format.json { render json: @all_invoice }
    end
  end

  private

  def get_merchant
    @merchant = Merchant.where(:id => params[:merchant_id]).first
  end

end
