class Epsadmin::DevicesController < ApplicationController
  # GET /devices
  # GET /devices.json
  before_action :get_merchant
  before_filter :check_not_merchant
  def index
    add_breadcrumb "Merchants", epsadmin_merchants_path
    add_breadcrumb "#{@merchant.first_name}", epsadmin_merchant_path(@merchant.id)
    add_breadcrumb "Devices","#"
    @epsadmin_devices = @merchant.devices
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @epsadmin_devices }
    end
  end

  # GET /devices/1
  # GET /devices/1.json
  def show
    @epsadmin_device = Device.find(params[:id])
    add_breadcrumb "Merchants", epsadmin_merchants_path
    add_breadcrumb "#{@epsadmin_device.merchant.first_name}", epsadmin_merchant_path(@epsadmin_device.merchant.id)
    add_breadcrumb "Devices",epsadmin_merchant_devices_path(@merchant.id)
    add_breadcrumb "#{@epsadmin_device.terminal_id}",""
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @epsadmin_device }
    end
  end

  # GET /devices/new
  # GET /devices/new.json
  def new
    add_breadcrumb "Merchants", epsadmin_merchants_path
    add_breadcrumb "#{@merchant.first_name}", epsadmin_merchant_path(@merchant.id)
    add_breadcrumb "Devices",epsadmin_merchant_devices_path(@merchant.id)
    add_breadcrumb "Add New Device",""
    @epsadmin_device = Device.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @epsadmin_device }
    end
  end

  # GET /devices/1/edit
  def edit
    @epsadmin_device = Device.find(params[:id])
    add_breadcrumb "Merchants", epsadmin_merchants_path
    add_breadcrumb "#{@epsadmin_device.merchant.first_name}", epsadmin_merchant_path(@epsadmin_device.merchant.id)
    add_breadcrumb "Devices",epsadmin_merchant_devices_path(@merchant.id)
    add_breadcrumb "#{@epsadmin_device.terminal_id}",epsadmin_merchant_device_path
    add_breadcrumb "Edit",""
  end

  # POST /devices
  # POST /devices.json
  def create
    add_breadcrumb "Merchants", epsadmin_merchants_path
    add_breadcrumb "#{@merchant.first_name}", epsadmin_merchant_path(@merchant.id)
    add_breadcrumb "Devices",epsadmin_merchant_devices_path(@merchant.id)
    add_breadcrumb "Add New Device",""
    @epsadmin_device = Device.new(device_params)
     @epsadmin_device.modifier_id=current_user.id
    #Generation of terminal id
    @epsadmin_device.terminal_id = params[:device][:device_serial_number].last(3) + params[:device][:merchant_id].first(3)
    #End
    respond_to do |format|
      if @epsadmin_device.save
        format.html { redirect_to epsadmin_merchant_devices_path, notice: 'Device was successfully created.' }
        format.json { render json: epsadmin_merchant_devices_path, status: :created, location: @epsadmin_device }
      else
        format.html { render action: "new" }
        format.json { render json: @epsadmin_device.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /devices/1
  # PUT /devices/1.json
  def update
    @epsadmin_device = Device.find(params[:id])
    add_breadcrumb "Merchants", epsadmin_merchants_path
    add_breadcrumb "#{@epsadmin_device.merchant.first_name}", epsadmin_merchant_path(@epsadmin_device.merchant.id)
    add_breadcrumb "Devices",epsadmin_merchant_devices_path(@merchant.id)
    add_breadcrumb "#{@epsadmin_device.terminal_id}",""
    add_breadcrumb "Edit",""
    respond_to do |format|
      if @epsadmin_device.update_attributes(device_params)
        format.html { redirect_to epsadmin_merchant_device_path, notice: 'Device was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: epsadmin_merchant_device_path.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /devices/1
  # DELETE /devices/1.json
  def destroy
    @epsadmin_device = Device.find(params[:id])
    if @epsadmin_device.device_status == "pending"
      @epsadmin_device.destroy
      respond_to do |format|
        format.html { redirect_to epsadmin_merchant_devices_path }
        format.json { head :no_content }
      end
    else
        respond_to do |format|
          format.html { redirect_to epsadmin_merchant_device_path, notice: 'Device status must be in pending state to delte/remove it' }
          format.json { head :no_content }
        end
    end
  end

  def device_status_check
    mid=Device.where(:id=>params[:device_id]).first
    if params[:st] == "activate"
      @res=mid.activate
    elsif params[:st] == "inactivate"
      @res=mid.inactivate
    elsif params[:st] == "deactivate"
      @res=mid.deactivate
    else
      @res=mid
    end 
    respond_to do |format|
      format.html
      format.json { render :json=>@res} 
    end
  end

  private

  def device_params
    params.require(:device).permit(:device_serial_number, :sim_phone_number, :sim_msid_number, :device_make, :device_display, :bank_mmid, :device_status, :terminal_id, :setup_cost, :monthly_cost, :transaction_cost, :parent_id, :merchant_id,:modifier_id,:version_comments)
  end

  def get_merchant
    @merchant = Merchant.where(:id => params[:merchant_id]).first
  end

end
