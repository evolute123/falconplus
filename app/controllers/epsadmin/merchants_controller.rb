class Epsadmin::MerchantsController < ApplicationController
  # GET /merchants
  # GET /merchants.json
  before_action :get_merchant, :get_device
  before_filter :check_not_merchant

  def index
    add_breadcrumb "Merchants", epsadmin_merchants_path
    @epsadmin_merchants = Merchant.all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @epsadmin_merchants }
    end
  end

  # GET /merchants/1
  # GET /merchants/1.json
  def show
    @epsadmin_merchant = Merchant.find(params[:id])
    add_breadcrumb "Merchants", epsadmin_merchants_path
    add_breadcrumb "#{@epsadmin_merchant.first_name}", ""
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @epsadmin_merchant }
    end
  end

  # GET /merchants/new
  # GET /merchants/new.json
  def new
    add_breadcrumb "Merchants", epsadmin_merchants_path
    add_breadcrumb "New", ""
    @epsadmin_merchant = Merchant.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @epsadmin_merchant }
    end
  end

  # GET /merchants/1/edit
  def edit
    @epsadmin_merchant = Merchant.find(params[:id])
    add_breadcrumb "Merchants", epsadmin_merchants_path
    add_breadcrumb "#{@epsadmin_merchant.first_name}", epsadmin_merchant_path
    add_breadcrumb "Edit", ""
  end

  # POST /merchants
  # POST /merchants.json
  def create
    add_breadcrumb "Merchants", epsadmin_merchants_path
    add_breadcrumb "New", ""
    @epsadmin_merchant = Merchant.new(merchant_params)
     @epsadmin_merchant.modifier_id=current_user.id
    
      if params[:merchant][:is_kyc_submitted] == "1"
        if !params[:merchant][:address_proof].blank? && !params[:merchant][:given_address_proof].blank? && !params[:merchant][:id_proof].blank? && !params[:merchant][:given_id_proof].blank? && !params[:merchant][:business_id_proof].blank? && !params[:merchant][:given_business_id_proof].blank? && !params[:merchant][:business_address_proof].blank? && !params[:merchant][:given_business_address_proof].blank?
          respond_to do |format|

            
            if @epsadmin_merchant.save
              @subscription = Subscription.new(:start_subscription_date => params[:start_subscription_date],:monthly_invoice_date => params[:start_subscription_date], :merchant_id =>  @epsadmin_merchant.id )
              @subscription.save if @subscription
              create_user(params[:merchant][:status],@epsadmin_merchant)
              format.html { redirect_to epsadmin_merchants_path, notice: 'Merchant was successfully created.' }
              format.json { render json: @epsadmin_merchant, status: :created, location: @epsadmin_merchant }
            else
              format.html { render action: "new" }
              format.json { render json: @epsadmin_merchant.errors, status: :unprocessable_entity }
            end
          end
        else
          respond_to do |format|
            format.html { render action: "new" }
            @epsadmin_merchant.errors.add(:is_kyc_submitted, 'must be unchecked due to some documents not uploaded/seleted')
          end
        end
      else
        respond_to do |format|
          if @epsadmin_merchant.save
            @subscription = Subscription.new(:start_subscription_date => params[:start_subscription_date] ,:monthly_invoice_date => params[:start_subscription_date], :merchant_id =>  @epsadmin_merchant.id )
            @subscription.save if @subscription
            create_user(params[:merchant][:status],@epsadmin_merchant)
            format.html { redirect_to epsadmin_merchants_path, notice: 'Merchant was successfully created.' }
            format.json { render json: @epsadmin_merchant, status: :created, location: @epsadmin_merchant }
          else
            format.html { render action: "new" }
            format.json { render json: @epsadmin_merchant.errors, status: :unprocessable_entity }
          end
        end
      end
  end

  # PUT /merchants/1
  # PUT /merchants/1.json
  def update
    @epsadmin_merchant = Merchant.find(params[:id])
    add_breadcrumb "Merchants", epsadmin_merchants_path
    add_breadcrumb "#{@epsadmin_merchant.first_name}", epsadmin_merchant_path
    respond_to do |format|
      if @epsadmin_merchant.update_attributes(merchant_params)
        @epsadmin_merchant.update_attributes(:version_comments => params[:version_comments])
        @subscription_update = @epsadmin_merchant.subscription.update_attributes(:start_subscription_date => params[:start_subscription_date] ,:monthly_invoice_date => params[:start_subscription_date], :merchant_id =>  @epsadmin_merchant.id )
        create_user(params[:merchant][:status],@epsadmin_merchant)
        format.html { redirect_to epsadmin_merchant_path, notice: 'Merchant was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @epsadmin_merchant_path.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /merchants/1
  # DELETE /merchants/1.json
  def destroy
    @epsadmin_merchant = Merchant.find(params[:id])
    @epsadmin_merchant.destroy
    respond_to do |format|
      format.html { redirect_to epsadmin_merchants_url, notice: 'Merchant was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  #For getting transaction list
  def transaction
    add_breadcrumb "Merchants", epsadmin_merchants_path
    add_breadcrumb "#{@merchant.first_name}", epsadmin_merchant_path(@merchant.id)
    add_breadcrumb "Transactions", ""
    @epsadmin_transaction_list = @merchant.transactions
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @epsadmin_transaction_list }
    end
  end

  def smart_listing_resource
    @merchant ||= params[:id] ? Merchant.find(params[:id]) : Merchant.new(params[:merchant])
  end
  helper_method :smart_listing_resource

  def smart_listing_collection
    @merchants ||= Merchant.all
  end
  helper_method :smart_listing_collection

  def merchant_status_check
    mid=Merchant.where(:id=>params[:merchant_id]).first
    if params[:st] == "activate"
      @res=mid.activate
    elsif params[:st] == "inactivate"
      @res=mid.inactivate
    elsif params[:st] == "close"
      @res=mid.close
    elsif params[:st] == "cancel"
      @res=mid.cancel
    else
      @res=mid
    end 
    respond_to do |format|
      format.html
      format.json { render :json=>@res} 
    end
  end

  def transaction_status_check
    mid=Transaction.where(:id=>params[:tid]).first
    if params[:cls] == "btn btn-danger cancel"
      @res=mid.cancel
    else 
      mid.update_attributes(:payment_timestamp => params[:date] ,:transaction_details => params[:details], :transaction_type =>  params[:type])
      @res=mid.pay
    end
    respond_to do |format|
      format.html
      format.json { render :json=>@res} 
   end
  end

  
  private
    # Use callbacks to share common setup or constraints between actions.
  def set_merchant
    @epsadmin_merchant = Merchant.find(params[:id])
  end

    # Never trust parameters from the scary internet, only allow the white list through.
  def merchant_params
    params.require(:merchant).permit(:first_name,:last_name,:email_address,:phone_number,:age,:sex,:business_name,:business_type,:industry_type,:business_address,:is_kyc_submitted,:address_proof,:given_address_proof,:id_proof,:given_id_proof,:business_id_proof, :business_address_proof,:version_comments,:modifier_id,:given_business_address_proof,:given_business_id_proof,:alloted_devices, :merchant_id,:status, photos_attributes: :id_proof, photos_attributes: :address_proof, photos_attributes: :business_address_proof, photos_attributes: :business_id_proof)
  end
  
  def create_user(status,merchant)
    if status.gsub(" ","").downcase == "approve"
      if epsadmin_merchant.user.nil?
        user = User.create(:email => epsadmin_merchant.email_address,:password => EpsAdmin::User.generate_password)
        # epsadmin_merchant.update_attributes(:user_id => user.id)
       UserMailer.sms_user(user).deliver
      end
    end
  end

  def get_merchant
    @merchant = Merchant.where(:id => params[:merchant_id]).first
  end

  def get_device
    @device = Device.where(:merchant_id => params[:merchant_id]).first
  end

end


