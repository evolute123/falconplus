module ApplicationHelper

	def is_merchant_editable?(status)
	    status = Merchant.where(:status=>status).first.status
	    if status.include?('pending') || status.include?('inactive')
	    	return true
	    else
	    	return false
	    end
  	end

  	def is_merchant_deletable?(status)
	    status = Merchant.where(:status=>status).first.status
	    if status.include?('pending')
	    	return true
	    else
	    	return false
	    end
  	end

  	def is_device_addable?(status)
	    status = Merchant.where(:status=>status).first.status
	    if status.include?('pending') || status.include?('active')
	    	return true
	    else
	    	return false
	    end
	end

	def is_invoice_addable?(status)
		status = Merchant.where(:status=>status).first.status
	    if status.include?('pending') || status.include?('active') || status.include?('inactive')
	    	return true
	    else
	    	return false
	    end
	end
	
	def is_device_editable?(status)
		status = Device.where(:device_status=>status).first.device_status
	    if status.include?('pending')
	    	return true
	    else
	    	return false
	    end
	end

	def is_device_invoice_addable?(status)
		status = Device.where(:device_status=>status).first.device_status
	    if status.include?('pending')
	    	return true
	    else
	    	return false
	    end
	end

	def is_device_deletable?(status)
		status = Device.where(:device_status=>status).first.device_status
	    if status.include?('pending')
	    	return true
	    else
	    	return false
	    end
	end

	def is_invoice_editable?(status)
		status = Invoice.where(:status=>status).first.status
	    if status.include?('pending')
	    	return true
	    else
	    	return false
	    end
	end

	def is_invoice_deletable?(status)
		status = Invoice.where(:status=>status).first.status
	    if status.include?('pending')
	    	return true
	    else
	    	return false
	    end
	end


end
